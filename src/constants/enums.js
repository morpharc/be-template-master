module.exports = {
    contractStatuses: {
        NEW: 'new',
        IN_PROGRESS: 'in_progress',
        TERMINATED: 'terminated',
    },
    profileTypes: {
        CLIENT: 'client',
        CONTRACTOR: 'contractor',
    },
};
