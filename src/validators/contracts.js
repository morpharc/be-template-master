const { Joi } = require('celebrate');

module.exports = {
    getContractById: {
        params: Joi.object({
            id: Joi.string().required(),
        }).options({ stripUnknown: true }),
    },
};
