const { Op } = require('sequelize');
const { Contract, Profile, Job, sequelize } = require('../model');
const errors = require('../constants/errors');
const { profileTypes } = require('../constants/enums');


class IndexService {
    async depositMoney ({ userId, amount }) {
        await sequelize.transaction(async (t) => {
            const client = await Profile.findOne({
                where: {
                    id: userId,
                },
            })
            if (!client) {
                throw new Error(errors.profile_not_found.code);
            }
            if (client.type !== profileTypes.CLIENT) {
                throw new Error(errors.wrong_user_type.code);
            }

            const amountToPay = await Contract.findAll({
                where: {
                    clientId: userId,
                },
                attributes: [
                    [sequelize.fn('sum', sequelize.col('Jobs.price')), 'amountToPay'],
                ],
                include: {
                    attributes: [],
                    model: Job,
                    where: {
                        paid: {
                            [Op.not]: true,
                        },
                    },
                },
                transaction: t,
                raw: true,
            }).then((res) => res[0]?.amountToPay);

            if ((amountToPay * 0.25) < amount) {
                throw new Error(errors.too_much_money.code);
            }

            client.balance = client.balance + amount;
            await client.save({ transaction: t });
        });
    };

}

module.exports = IndexService;
