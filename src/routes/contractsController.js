const { celebrate } = require('celebrate');
const { getProfile } = require('../middleware/getProfile');
const { getErrorData } = require('../modules');
const validator = require('../validators/contracts');
const ContractService = require('../services/contractService');

class ContractsController {
    constructor (router) {
        this.router = router;
        this.service = new ContractService();
    }

    getContractById () {
        this.router
            .route('/:id')
            .get(celebrate(validator.getContractById), getProfile, async (req, res) => {
                try {
                    const { id } = req.params;
                    const contract = await this.service.getContractById({ id, profile: req.profile });
                    return res.send(contract);
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    };

    getAllContracts () {
        this.router
            .route('/')
            .get(getProfile, async (req, res) => {
                try {
                    const contracts = await this.service.getAllContracts({ profile: req.profile });
                    return res.send(contracts);
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    }

    getRouter () { return this.router; }
}
module.exports = {
    forFeature: (router) => {
        const contractsController = new ContractsController(router);
        contractsController.getContractById();
        contractsController.getAllContracts();
        return contractsController.getRouter();
    }
};
