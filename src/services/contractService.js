const { Op } = require('sequelize');
const { Contract } = require('../model');
const errors = require('../constants/errors');
const { contractStatuses } = require('../constants/enums');

class ContractService {
    async getContractById ({ id, profile }) {
        const { id: profileId } = profile;
        const contract = await Contract.findOne({
            where: {
                id,
                [Op.or]: [
                    { ContractorId: profileId },
                    { ClientId: profileId },
                ],
            },
        });
        if (!contract) {
            throw new Error(errors.contract_not_found.code);
        }
        return contract;
    };

    async getAllContracts ({ profile }) {
        const { id: profileId } = profile;
        return await Contract.findAll({
            where: {
                status: {
                    [Op.ne]: contractStatuses.TERMINATED,
                },
                [Op.or]: [
                    { ContractorId: profileId },
                    { ClientId: profileId },
                ],
            },
        });
    };

}

module.exports = ContractService;
