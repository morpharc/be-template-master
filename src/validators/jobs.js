const { Joi } = require('celebrate');

module.exports = {
    payForAJob: {
        params: Joi.object({
            job_id: Joi.string().required(),
        }).options({ stripUnknown: true }),
    },
};
