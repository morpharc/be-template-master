const express = require('express');
const bodyParser = require('body-parser');
const { errors } = require('celebrate');
const { sequelize } = require('./model');
const applyRoutes = require('./routes');
const app = express();
app.use(bodyParser.json());
app.set('sequelize', sequelize);

applyRoutes(app);
app.use(errors());

module.exports = app;
