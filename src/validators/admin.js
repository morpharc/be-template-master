const { Joi } = require('celebrate');

module.exports = {
    getBestProfession: {
        query: Joi.object({
            start: Joi.date(),
            end: Joi.date(),
        }).options({ stripUnknown: true }),
    },
    getBestClients: {
        query: Joi.object({
            start: Joi.date(),
            end: Joi.date(),
            limit: Joi.number()
                .positive()
                .default(2),
        }).options({ stripUnknown: true }),
    },
};
