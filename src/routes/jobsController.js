const { celebrate } = require('celebrate');
const { getProfile } = require('../middleware/getProfile');
const { getErrorData } = require('../modules');
const validator = require('../validators/jobs');
const JobsService = require('../services/jobsService');

class JobsController {
    constructor (router) {
        this.router = router;
        this.service = new JobsService();
    }

    getUnpaidJobs () {
        this.router
            .route('/unpaid')
            .get(getProfile, async (req, res) => {
                try {
                    const jobs = await this.service.getUnpaidJobs({ profile: req.profile });
                    return res.send(jobs);
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    };

    payForAJob () {
        this.router
            .route('/:job_id/pay')
            .post(celebrate(validator.payForAJob), getProfile, async (req, res) => {
                try {
                    const { job_id } = req.params;
                    await this.service.payForAJob({ job_id, profile: req.profile });
                    return res.send();
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    };

    getRouter () { return this.router; }
}
module.exports = {
    forFeature: (router) => {
        const jobsController = new JobsController(router);
        jobsController.getUnpaidJobs();
        jobsController.payForAJob();
        return jobsController.getRouter();
    }
};
