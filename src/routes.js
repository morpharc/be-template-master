const express = require('express');
const indexRoutes = require('./routes/indexController');
const contractsRoutes = require('./routes/contractsController');
const jobsRoutes = require('./routes/jobsController');
const adminRoutes = require('./routes/adminController');

module.exports = (app) => {
    app.use('/v1/', indexRoutes.forFeature(express.Router({ mergeParams: true })));
    app.use('/v1/contracts', contractsRoutes.forFeature(express.Router({ mergeParams: true })));
    app.use('/v1/jobs', jobsRoutes.forFeature(express.Router({ mergeParams: true })));
    app.use('/v1/admin', adminRoutes.forFeature(express.Router({ mergeParams: true }))); //
};
