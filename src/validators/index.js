const { Joi } = require('celebrate');

module.exports = {
    deposit: {
        params: Joi.object({
            userId: Joi.string().required(),
        }).options({ stripUnknown: true }),
        body: Joi.object({
            amount: Joi.number()
                .positive()
                .required(),
        }).options({ stripUnknown: true })
    },
};
