const { celebrate } = require('celebrate');
const { getErrorData } = require('../modules');
const validator = require('../validators');
const IndexService = require('../services/indexService');

class IndexController {
    constructor (router) {
        this.router = router;
        this.service = new IndexService();
    }

    depositMoney () {
        this.router
            .route('/balances/deposit/:userId')
            .post(celebrate(validator.deposit), async (req, res) => {
                try {
                    const { userId } = req.params;
                    const { amount } = req.body;
                    await this.service.depositMoney({ userId, amount });
                    return res.send(true);
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    };

    getRouter () { return this.router; }
}
module.exports = {
    forFeature: (router) => {
        const indexController = new IndexController(router);
        indexController.depositMoney();
        return indexController.getRouter();
    }
};
