const { celebrate } = require('celebrate');
const { getErrorData } = require('../modules');
const validator = require('../validators/admin');
const AdminService = require('../services/adminService');

class AdminController {
    constructor (router) {
        this.router = router;
        this.service = new AdminService();
    }

    getBestProfession () {
        this.router
            .route('/best-profession')
            .get(celebrate(validator.getBestProfession), async (req, res) => {
                try {
                    const { start, end } = req.query;
                    const profession = await this.service.getBestProfession({ start, end });
                    return res.send(profession);
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    };

    getBestClients () {
        this.router
            .route('/best-clients')
            .get(celebrate(validator.getBestClients), async (req, res) => {
                try {
                    const { start, end, limit } = req.query;
                    const profession = await this.service.getBestClients({ start, end, limit });
                    return res.send(profession);
                } catch (err) {
                    const { status, message } = getErrorData(err);
                    res.status(status).send(message);
                }
            });
    };

    getRouter () { return this.router; }
}
module.exports = {
    forFeature: (router) => {
        const adminController = new AdminController(router);
        adminController.getBestProfession();
        adminController.getBestClients();
        return adminController.getRouter();
    }
};
