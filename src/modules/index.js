const errors = require('../constants/errors');

const getErrorData = (err) => {
    const { status, message } = errors[err.message] || errors.invalid_request;
    return { status, message };
};

module.exports = {
    getErrorData,
}
