const { Op } = require('sequelize');
const { Contract, Profile, Job, sequelize } = require('../model');
const errors = require('../constants/errors');
const { contractStatuses, profileTypes } = require('../constants/enums');


class JobsService {
    async getUnpaidJobs ({ profile }) {
        const { id: profileId } = profile;
        const jobs = await Job.findAll({
            where: {
                paid: {
                    [Op.not]: true,
                },
            },
            include: {
                model: Contract,
                where: {
                    status: contractStatuses.IN_PROGRESS,
                    [Op.or]: [
                        { ContractorId: profileId },
                        { ClientId: profileId },
                    ],
                },
            },
        })
        return jobs;
    };

    async payForAJob ({ job_id, profile }) {
        const { id: profileId, balance, type } = profile;
        if (type !== profileTypes.CLIENT) {
            throw new Error(errors.wrong_user_type.code);
        }
        const job = await Job.findOne({
            where: {
                id: job_id,
            },
            include: {
                model: Contract,
                where: {
                    clientId: profileId,
                },
            },
        });
        if (!job) {
            throw new Error(errors.job_not_found.code);
        }
        if (balance < job.price){
            throw new Error(errors.insufficient_funds.code);
        }
        await sequelize.transaction(async (t) => {
            const jobPrice = job.price || 0;

            job.paid = true;
            job.paymentDate = new Date();
            await job.save({ transaction: t });

            await Profile.update({
                balance: balance - jobPrice,
            }, {
                where: {
                    id: profileId,
                },
                transaction: t,
            });

            const contractor = await Profile.findOne({
                where: {
                    id: job?.Contract?.ContractorId || 0,
                },
                transaction: t,
            });
            if (!contractor) {
                throw new Error(errors.profile_not_found.code)
            }
            contractor.balance = contractor.balance + jobPrice;
            await contractor.save({ transaction: t });
        });
    };

}

module.exports = JobsService;
