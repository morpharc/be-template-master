module.exports = {
    invalid_request: {
        code: 'invalid_request',
        status: 400,
        message: { tsl: 'invalid_request' }
    },
    contract_not_found: {
        code: 'contract_not_found',
        status: 404,
        message: { tsl: 'contract_not_found' }
    },
    job_not_found: {
        code: 'job_not_found',
        status: 404,
        message: { tsl: 'job_not_found' }
    },
    profile_not_found: {
        code: 'profile_not_found',
        status: 404,
        message: { tsl: 'profile_not_found' }
    },
    wrong_user_type: {
        code: 'wrong_user_type',
        status: 409,
        message: { tsl: 'wrong_user_type' }
    },
    too_much_money: {
        code: 'too_much_money',
        status: 409,
        message: { tsl: 'too_much_money' }
    },
    insufficient_funds: {
        code: 'insufficient_funds',
        status: 409,
        message: { tsl: 'insufficient_funds' }
    },
};
