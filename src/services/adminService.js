const { Op } = require('sequelize');
const { Contract, Profile, Job, sequelize } = require('../model');

class AdminService {
    async getBestProfession ({ start, end }) {
        const bestProfessions = await Contract.findAll({
            where: {
                [Op.and]: {
                    ...start && { createdAt: { [Op.gte]: new Date(start) } },
                    ...end && { createdAt: { [Op.lte]: new Date(end) } },
                }
            },
            attributes: [
                [sequelize.col('Contractor.profession'), 'profession'],
                [sequelize.fn('sum', sequelize.col('Jobs.price')), 'totalAmount'],
            ],
            include: [
                {
                    attributes: [],
                    model: Profile,
                    as: 'Contractor',
                },
                {
                    attributes: [],
                    model: Job,
                    where: {
                        paid: {
                            [Op.eq]: true,
                        },
                        [Op.and]: {
                            ...start && { createdAt: { [Op.gte]: new Date(start) } },
                            ...end && { createdAt: { [Op.lte]: new Date(end) } },
                        }
                    }
                },
            ],
            group: ['ContractorId'],
            order: [['totalAmount', 'DESC']],
            raw: true,
        });
        return bestProfessions[0]?.profession;
    };

    async getBestClients ({ start, end, limit = 2 }) {
        const clients = await Contract.findAll({
            where: {
                [Op.and]: {
                    ...start && { createdAt: { [Op.gte]: new Date(start) } },
                    ...end && { createdAt: { [Op.lte]: new Date(end) } },
                }
            },
            attributes: [
                [sequelize.col('Client.id'), 'id'],
                [sequelize.col('Client.firstName'), 'firstName'],
                [sequelize.col('Client.lastName'), 'lastName'],
                [sequelize.fn('sum', sequelize.col('Jobs.price')), 'totalAmount'],
            ],
            include: [
                {
                    attributes: [],
                    model: Profile,
                    as: 'Client',
                },
                {
                    attributes: [],
                    model: Job,
                    where: {
                        paid: {
                            [Op.eq]: true,
                        },
                        [Op.and]: {
                            ...start && { createdAt: { [Op.gte]: new Date(start) } },
                            ...end && { createdAt: { [Op.lte]: new Date(end) } },
                        }
                    }
                },
            ],
            group: ['ClientId'],
            order: [['totalAmount', 'DESC']],
            subQuery: false,
            raw: true,
            limit,
        });
        return clients;
    };
}

module.exports = AdminService;
